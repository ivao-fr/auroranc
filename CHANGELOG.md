# Aurora Nouvelle Calédonie Changelog #

## Update 07 February 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **Colorscheme (clr)** : FRANCE
- **FIXES files (fix)** : NWWW
- **VOR files (vor)** : NWWW
- **NDB files (ndb)** : NWWW
- **AWY files (hawy/lawy)** : NWWW
- **APT files (apt)** : NWWW
- **RWY files (rwy)** : NWWW
- **ATC file (atc)** : AFIS, NWWL<sup>X</sup>
- **TMA files (hartcc)** : NWWW, NWWM<sup>X</sup>
- **CTR files (lartcc)** : NWWW, NWWL<sup>X</sup>, NWWM
- **SID files (sid)** : NWWW, NWWM
- **STAR files (str)** : NWWC, NWWE, NWWD, NWWK, NWWL, NWWR, NWWW, NWWM, NWWV, NWWU
- **VFR fixes files (vfi)** : NWWE, NWWM

---
### Added Files ###

- **Symbols file (sym)** : FR
- **ATIS file (atis)** : France
- **STAR files (str)** : NLWF, NLWW
- **VFR fixes files (vfi)** : NWWD

<sup>X</sup> : File content has been withdrawn and file will be deleted in the next update.

---
---


## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**Initial Release**